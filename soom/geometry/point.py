# -*- coding: utf-8 -*-
"""
Point
~~~~~

:synopsis: Handles 3D points in space.
"""


class Point:
    """
    :class:`Point` allows the creation of 3D points in space.

        Also, some legacy functions exist at this time and may be deprecated
        in the near future.

    Attributes:
        coords (list): X, Y, Z coordinates to build from
        comment (str): Optional comments
        vhandle (list): List of objects for graphical representation
        
    """
    def __init__(self, coords=[0, 0, 0], comment="", number=-1):
        """
        Constructor for Point objects.

        Args:
            coords (list): X, Y, Z coordinates for the point
            comment (str): Optional comments
            vhandle (list): List of objects for graphical representation
            number(int): Integer identifying the point.  Default to -1
        """
        self.coords  = coords
        self.comment = comment
        self.vhandle = 0
        self.number = number

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string
        """
        out = "Point:\n"
        out += "X = " + str(self.coords[0]) + '\n'
        out += "Y = " + str(self.coords[1]) + '\n'
        out += "Z = " + str(self.coords[2]) + '\n'
        out += "Comments: " + self.comment
        return out
        
    def display(self):
        """
        Renders this Point instance as a string.

        Returns:
            str. The string representation of this Point instance.
        """
        return str(self)

    def plot(self, number=False):
        """
        Plots this Point instance via Matplotlib.
        
        Args:
            number (bool): True to plot numbers.  Default is False

        Returns:
            None.
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

        # Set projection on current figure to 3D.
        fig = plt.gcf()
        ax = fig.gca(projection='3d')
        
        # Coords, color, display symbol
        x = self.coords[0]
        y = self.coords[1]
        z = self.coords[2]
        self.vhandle = ax.plot([x], [y], [z], 'ro')
        
        # Plot numbers
        ax.text(x,y,z,' %i'%self.number)

# Extracts all points from a 3xN numpy array.
def point_extract(pointArray):
    """
    Legacy code. May be deprecated in future versions.
    """
    pass


# Find closest-matching point(s) in a list of points. To match a specific
# Point() object's coords, just call it like: `find(pointList, a.coords)`.
def point_find(pointList, coords, closest=False, p=1):
    """
    Legacy code. May be deprecated in future versions.
    """
    out = []
    for pt in pointList:
        # For exact matching behavior, set distance tolerance to 0.
        if closest is False:
            p = 0

        save = True

        # Absolute distance from value. If distance is greater than tolerance
        # 'p' in both directions, reject this point.
        if abs(coords[0] - pt.coords[0]) > p:
            save = False
        if abs(coords[1] - pt.coords[1]) > p:
            save = False
        if abs(coords[2] - pt.coords[2]) > p:
            save = False

        # If the point was not rejected, append it to the output list:
        if save is True:
            out.append(pt)

    return out


# Tests __str__ method.
if __name__ == '__main__':
    a = Point([1,2,3])
    print a

    # plotting test
    fig = plt.figure()
    ax = Axes3D(fig) 
    a.plot()
    plt.show()

