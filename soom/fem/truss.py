# -*- coding: utf-8 -*-
"""
Truss
~~~~~

:synopsis: Creates truss elements (axial forces only).

Using Truss
-----------

Trusses are objects that can only be in tension or compression (axial forces).
An example of a truss can be seeing in the following example:

>>> nlist = [ Node([0,0,0],bc=[True]*6), Node([0,1,1],bc=[True]*6), Node([0,1,0])]
>>> s = Section(area = 0.1)
>>> mat = Isolinear(name = "Steel",e = 2e11, rho = 7850)
>>> ele = [Truss([nlist[0], nlist[1]],s,m), Truss([nlist[1],nlist[2]],s,m), Truss([nlist[0],nslist[0]],s,m)]

Assumptions
-----------

The truss (or bar) element considers the following assumptions:

* The element is straight
* The material is :class:`soom.materials.isolinear.Isolinear`
* Forces are only applied at the end of the bar
* The bar supports only axial load 

Degrees of freedom
------------------

The degrees of freedom of the truss or bar element are described by the vector:

.. math::

    d^T = \\left\\{ \\begin{matrix}
    u_1, v_1, w_1, u_2, v_2, w_2
    \\end{matrix} \\right\\}^T

where :math:`u` correspond to displacement in the :math:`x` direction; :math:`v`
indicates displacement in the :math:`y` direction; and :math:`w` corresponds
to displacement in the :math:`z` direction.  The sub-index (1 or 2) indicate the
first or second node from :attr:`Truss.nodeList`.

Stiffness matrix
----------------

The stiffness matrix in local coordinates can be calculated using :meth:`Truss.localk`.
The local stiffness matrix is given by the equation [Cook_et_al_2007]_ :

.. math::

    K = {A E \\over L}\\left[ \\begin{matrix}
    1 & 0 & 0 & -1 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 \\\\
    -1 & 0 & 0 & 1 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 \\\\    
    \\end{matrix} \\right]

Where :math:`A` is the cross sectional area of the cross section, :math:`E` is
the Young modulus of the materia, and :math:`L` is the length of the element.

The stiffness matrix in global coordinates can be calculated using :meth:`Truss.globalk()`.
The transformation matrix is inherited from :class:`soom.geometry.fline.Fline`.
For more information about the coordinate transformation see the 
:ref:`Fline coordinate transofrmation <fline-coordinate-transformation>` section.

Mass matrix
-----------

The mass matrix in local coordinates can be calculated using the :meth:`Truss.localm` method.
The mass matrix is calculated as:

.. math::

    M = {\\rho A L \\over 6} \\left[ \\begin{matrix}
    2 & 0 & 0 & 1 & 0 & 0 \\\\
    0 & 2 & 0 & 0 & 1 & 0 \\\\
    0 & 0 & 2 & 0 & 0 & 1 \\\\
    1 & 0 & 0 & 2 & 0 & 0 \\\\
    0 & 1 & 0 & 0 & 2 & 0 \\\\
    0 & 0 & 1 & 0 & 0 & 2 \\\\        
    \end{matrix} \\right]

Where :math:`\\rho` is the density of the material, :math:`L` is the length of
the element, and :math:`A` is the area of the cross section.  The mass matrix
in global coordinates can be calculated using :meth:`Trus.globalm`.  For more 
information about the coordinate transformation see the 
:ref:`Fline coordinate transofrmation <fline-coordinate-transformation>` section.

Bibliography
------------

.. [Cook_et_al_2007] Cook, Robert D. `Concepts and applications of finite element analysis`. John Wiley & Sons, 2007.

"""

import numpy as np

from .node import Node
from ..geometry.fline import Fline
from ..geometry.section import Section

class Truss(Fline):
    """
    :class:`Truss` is used to model trusses in structures.

    Attributes:
        nodeList (list): List of nodes to build from.
        section (Section): Cross-section object.
        material (Isolinear): Material properties of the truss.
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 section,
                 material,
                 comment=""):
        """
        Constructor for Truss objects.

        Args:
            nodeList (list): List of nodes to build from.
            section (Section): Cross-section object.
            material (Isolinear): Material properties of the truss.
            comment (str): Optional comments.
        """
        super(Truss, self).__init__(nodeList=nodeList, comment=comment)
        self.section  = section
        self.material = material

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Truss:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n------\n'
        out += str(self.section) + '\n------\n'
        out += str(self.material) + '\n------\n'
        out += "Truss Comments: " + self.comment
        return out
        
    def display(self):
        """
        Renders the Truss instance as a string.

        Returns:
            str. The string representation of this Ebbeam instance.
        """
        return str(self)

    def localm(self):
        """
        Computes the local mass matrix of the truss.

        Returns:
            m (numpy.array): The local mass matrix.
        """
        # Material properties
        Rho = self.material.rho

        # Geometry properties
        Ar = self.section.area

        # Lenght property
        L = self.length()

        # some additional terms
        alpha = Rho*Ar*L/6
        m = np.zeros(shape=(6, 6))
        I = np.eye(6, 6)
        
        m[0,0] = 2*alpha
        m[0,3] = alpha
        m[1,1] = 2*alpha
        m[1,4] = alpha
        m[2,2] = 2*alpha
        m[2,5] = alpha
        m[3,3] = 2*alpha
        m[4,4] = 2*alpha
        m[5,5] = 2*alpha
        m = m+m.transpose()-np.multiply(I, m)        

        return m

    def localk(self):
        """
        Computes the local stiffnes matrix of the truss.

        Returns:
            k (numpy.array): The local stiffness matrix.
        """
        # Young modulus
        E = self.material.e

        # Area of the cross sectional section
        A  = self.section.area

        # Lenght
        L = self.length()

        alpha = A*E/L
        
        k = np.zeros(shape=(6, 6))
        k[0,0] = alpha
        k[0,3] = -alpha
        k[3,0] = -alpha
        k[3,3] = alpha
        
        return k
        
        
    def globalm(self):
        """
        Calculates the global mass matrix

        Returns:
            M (numpy.array): Global mass matrix.
        """
        # Calculating transformation and mass matrix in local coordinates
        T = self.tmatrix()
        
        # Select the appropriate parts of the transformation matrix
        T = np.delete(T,[3, 4, 5, 9, 10, 11],0)
        T = np.delete(T,[3, 4, 5, 9, 10, 11],1)
        
        # Calculate the mass matrix in local coordinates
        m = self.localm()

        # Calculate global mass matrix
        M = np.dot(np.dot(T.T, m), T)

        return M

    def globalk(self):
        """
        Calculates the global stiffness matrix

        Returns:
            K (scipy.array): Global stiffness matrix
        """
        # Calculating transformation and local stiffness matrix
        T = self.tmatrix()
        
        # Select the appropriate parts of the transformation matrix
        T = np.delete(T,[3, 4, 5, 9, 10, 11],0)
        T = np.delete(T,[3, 4, 5, 9, 10, 11],1)
        
        # Calculate the stiffness matrix in local coordinates
        k = self.localk()

        # Calculate global stiffness matrix
        K = np.dot(np.dot(T.T, k), T)

        return K
        
    def plot(self, plotopt = 'r'):
        """
        Plots this Truss instance via Matplotlib.
        
        The Truss.vhandle property is updated with a reference to the object
        graphically representing the Truss.  By default Truss elements are
        plotted red.

        Returns:
            None.

        """
        super(Truss, self).plot(plotopt = plotopt)
    
    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and stiffness

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*3, self.nodeList[0].number*3+3)
        dof += range(self.nodeList[1].number*3, self.nodeList[1].number*3+3)
        return dof

    def check(self):
        """
        Checks if the arguments of the truss have been set correctly

        The arguments that are checked are:

        * nodeList is a list of 2 elements of type Node
        * section is of type section

        Returns:
            checkFlag (boolean): True if the check passes
        """
        checkFlag = True

        # Check that the nodeList is a list of 3 nodes
        if type(self.nodeList) is not list:
            print "The truss.nodeList is expected to be a list (id %i)" % id(self)
            checkFlag = False
        elif len(self.nodeList) is not 2:
            print "The truss.nodeList is expected to have 2 elements (id %i)" % id(self)
            checkFlag = False
        elif not all([isinstance(item, Node) for item in self.nodeList]):
            print "The elements of truss.nodeList are expected to be Nodes (id %i)" % id(self)
            checkFlag = False

        # Checking that the section is type section
        if not isinstance(self.section, Section):
            print "The truss.section is expected to be type section (id %i)" % id(self)
            checkFlag = False
            
        return checkFlag
        