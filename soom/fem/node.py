# -*- coding: utf-8 -*-
"""
Node
~~~~

:synopsis: Handles 3D points in space.


Using Nodes
-----------

Nodes are used throughout SOOMpy to model 3D objects and structures.
To create a Node, one only needs to specify the 3 coordinates, x, y, and z in
a list, for example:

>>> a = Node([0, 1, 2])

Which creates a node at point X=0, Y=1, Z=2 in 3D space.

Nodes also have a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`Node.plot` method of a Node. For example:

>>> a.plot()

.. image:: images/ex_node_plot.png

The vhandle property references the virtual representation of this object
in the plot.  For example, if one would like to change the marker of the node
one can use

>>> a.set_marker('*')

In some cases it is necesary to "redraw" the plot using

>>> import matplotlib.pyplot as plt
>>> plt.draw()

:meth:`Node.plot` uses the argument number to plot the node number.  The
default is false.  For example:
    
>>> a.plot(number=True)

Coordinates of a Node may change over time. To manually change a Node's
coordinates, one must provide either a new set of coordinates, or must
modify a coordinate by index, for example:

>>> a.coords = [0, 0, 0] # set coordinates of Node a to 0,0,0.
>>> a.coords[0] = 2      # set Node a's x coordinate to 2.

To see the values stored in a Node, one has to call its :meth:`Node.__str__`
method. The easiest way to do this is to just ``print`` the Node in question
(``print`` calls :meth:`Node.__str__` behind the scenes). For example:

>>> print a
Node:
X = 2
Y = 0
Z = 0
Deformation = [0, 0, 0, 0, 0, 0]
Cdof = [0, 0, 0, 0, 0, 0]
Comments:

"""

from ..geometry.point import Point


class Node(Point):
    """
    :class:`Node` is used to model 3D points in space.

    Note:
        At this time, dependent properties are set when the object is created.
        In the future, this may be replaced by methods that compute the
        dependent properties on-demand.

        Also, some legacy functions exist at this time and may be deprecated
        in the near future.

    Attributes:
        coords (list): X, Y, Z coordinates for the node.
        deformation (list): Deformation vector.
        comment (str): Optional comments.
        bc (list): Boundary conditions.  The len(bc) = 6.  False = free DOF.
            True = constraint DOF.
        cdof (list): Condense degrees of freedom. The len(cdof) = 6.  A value
            of True indicates that the DOF is constrained
        number (int): Node number.  This can be automatically assigned by femmodel.numerateNodes()
    """
    def __init__(self,
                 coords,
                 bc=[False, False, False, False, False, False],
                 deformation=[0, 0, 0, 0, 0, 0],
                 comment="",
                 cdof=[False, False, False, False, False, False],
                 number=-1):
        """
        Constructor for Node objects.

        Args:
            coords (list): X, Y, Z coordinates for the node.
            bc (list): Boundary conditions
            deformation (list): Deformation vector.
            comment (str): Optional comments.
            vhandle (mpl_toolkits.mplot3d.art3d.Line3D): Reference to graphical
                representation
            cdof (list): Constrained degrees of freedom.
            number (int): Number identifying the DOF.  Default to -1
        """
        self.coords      = coords
        self.bc          = bc
        self.deformation = deformation
        self.comment     = comment
        self.cdof        = cdof
        self.number      = number
        self.vhandle     = 0

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Node:\n"
        out += "X = " + str(self.coords[0]) + '\n'
        out += "Y = " + str(self.coords[1]) + '\n'
        out += "Z = " + str(self.coords[2]) + '\n'
        out += "Deformation = " + str(self.deformation) + '\n'
        out += "Bc = " + str(self.bc) + '\n'
        out += "Cdof = " + str(self.cdof) + '\n'
        if self.number != -1:
            out += "Number = " + str(self.number) + "\n"
        out += "Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this Node instance as a string.

        Returns:
            str. The string representation of this Node instance.
        """
        return str(self)

    def check(self):
        """
        Checks if the parameters of the node are as expected

        Returns:
            checkFlag (boolean): True if the point parameters are as expected

        The method checks the following:

        * node.coords
            * Should be of type list
            * Length equal to 3
        * node.bc
            * Should be of type list
            * Length equal to 6
        * noce.cdof
            * Should be of type list
            * Length equal to 6
        """
        checkFlag = True

        # Checking that the coordinates is a list of 3 elements
        if type(self.coords) is not list:
            checkFlag = False
            print 'The node.coords property is expected to be a list (id %i)' % id(self)
        elif len(self.coords) is not 3:
            checkFlag = False
            print 'The length of the list node.coord is expected to be 3 (id %i)' % id(self)

        # Check that the bc list is a list of 6 elements
        if type(self.bc) is not list:
            checkFlag = False
            print "The node.bc property is expected to be a list (id %i)" % id(self)
        elif len(self.bc) is not 6:
            checkFlag = False
            print "The length of node.bc is expected to be 6 (id %i)" % id(self)

        # Check that the cdof is a list of 6 elements
        if type(self.cdof) is not list:
            checkFlag = False
            print "The node.cdof property is expected to be a list (id %i)" % id(self)
        elif len(self.cdof) is not 6:
            checkFlag = False
            print "The length of node.cdof is expected to be 6 (id %i)" % id(self)

        return checkFlag


def Node_mirror(obj, coord3, axis, option):
    """
    Legacy code. May be deprecated in future versions.
    """
    x, y, z = coord3  # Used multiple assignment
    mirrorAxis = ""

    if axis.upper in ['X', 'Y', 'Z']:
        mirrorAxis = axis.upper
    else:
        print "Error: Axis direction provided was not 'X', 'Y', or 'Z'."

    if mirrorAxis == 'X':
        x = 0  # 2*origin(1)-x;
    elif mirrorAxis == 'Y':
        y = 0  # 2*origin(2)-y;
    elif mirrorAxis == 'Z':
        z = 0  # 2*origin(3)-z;

    if option == "new":
        pass

    return Node([x, y, z])  # Will fail currently, since node class is brittle.
