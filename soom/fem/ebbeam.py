# -*- coding: utf-8 -*-
"""
Ebbeam
~~~~~~

:synopsis: Handles beam objects.


Using Ebbeam
------------

Ebbeams are used to model beams in 3D space.
To build an Ebbeam, one needs a list of Nodes, a cross-sectional area, and a
material, for example:

>>> nlist = [ Node([0,0,0]), Node([0,1,1]), Node([0,1,0]) ]
>>> s = Section(area = 0.1, j=1)
>>> mat = Isolinear(name = "Steel",e = 2e11, rho = 7850)
>>> beam = Ebbeam(nlist, s, mat)

This creates an Ebbeam from the list of provided Nodes. By convention, the
first two nodes define the beam itself, and all successive nodes are used to
compute orientation of the beam in 3D space.

Ebbeams have a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`Ebbeam.plot` method of an Ebbeam. For example:

>>> beam.plot()

.. image:: images/ex_ebbeam_plot.png

Properties of an Ebbeam may change over time. The nodes a beam is constructed
from can be changed by swapping out one node for another. This can be done by
indexing into the :attr:`Ebbeam.nodelist` member variable, for example:

>>> beam.nodeList[2] = Node([1,2,3])   # changes the node used for orientation.

To see the nodes and values stored in an Ebbeam, one has to call its
:meth:`Ebbeam.__str__` method. The easiest way to do this is to just ``print``
the Ebbeam in question (``print`` calls :meth:`Ebbeam.__str__` behind the
scenes). For example:

>>> print beam
Ebbeam:
Nodes: 3
- Node:
    X = 0
    Y = 0
    Z = 0
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Node:
    X = 0
    Y = 1
    Z = 1
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Node:
    X = 1
    Y = 2
    Z = 3
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
Length = Not Implemented
Dircosines = Not Implemented
Section = Not Implemented
Material = Not Implemented
T = Not Implemented
K = Not Implemented
M = Not Implemented
Comments:

Assumptions
-----------

The following assumptions are done with the Ebbeam element:

* Shear deformation is ignored

Degrees of freedom
------------------

.. image:: images/ebbeam_dof.png

The degrees of freedom of the truss or bar element are described by the vector:

.. math::

    d^T = \\left\\{ \\begin{matrix}
    u_1, v_1, w_1, \\theta_{x1}, \\theta_{y1}, \\theta_{z1}, u_2, v_2, w_2, \\theta_{x2}, \\theta_{y2}, \\theta_{z2}
    \\end{matrix} \\right\\}^T

where :math:`u` correspond to displacement in the :math:`x` direction; :math:`v`
indicates displacement in the :math:`y` direction; and :math:`w` corresponds
to displacement in the :math:`z` direction.  :math:`\\theta_x`, :math:`\\theta_y`, and :math:`\\theta_z`
are the rotations about the :math:`x`, :math:`y`, and :math:`z` axis respectively.
The sub-index (1 or 2) indicate the first or second node from :attr:`Truss.nodeList`.

Stiffness matrix
----------------

The stiffness matrix in local coordinates can be calculated using :meth:`Ebbeam.localk`.
The local stiffness matrix is given by the equation [Cook_et_al_2007]_ :

.. math::

    K =\\left[  \\begin{matrix}
    {A E \\over L} & 0 & 0 & 0 & 0 & 0 & -{A E \\over L} & 0 & 0 & 0 & 0 & 0 \\\\
    0 & {12 E I_y \\over L^3} & 0 & 0 & 0 & {6 E I_y \\over L^2} & 0 & -{12 E I_y \\over L^3} & 0 & 0 & 0 & {6 E I_y \\over L^2} \\\\
    0 & 0 & {12 E I_z \\over L^3} & 0 & {6 E I_z \\over L^2} & 0 & 0 & 0 & -{12 E I_z \\over L^3} & 0 & {6 E I_z \\over L^2} & 0 \\\\
    0 & 0 & 0 & {G J \\over L} & 0 & 0 & 0 & 0 & 0 & -{G J \\over L} & 0 & 0 \\\\
    0 & 0 & {6 E I_z \\over L^2} & 0 & {4 E I_z \\over L} & 0 & 0 & 0 & -{6 E I_z \\over L^2} & 0 & {2 E I_z \\over L} & 0 \\\\
    0 & {6 E I_y \\over L^2} & 0 & 0 & 0 & {4 E I_y \\over L} & 0 & -{6 E I_y \\over L^2} & 0 & 0 & 0 & {2 E I_y \\over L} \\\\
    - {A E \\over L} & 0 & 0 & 0 & 0 & 0 & {A E \\over L} & 0 & 0 & 0 & 0 & 0 \\\\
    0 & -{12 E I_y \\over L^3} & 0 & 0 & 0 & -{6 E I_y \\over L^2} & 0 & {12 E I_y \\over L^3} & 0 & 0 & 0 & -{6 E I_y \\over L^2} \\\\
    0 & 0 & -{12 E I_z \\over L^3} & 0 & -{6 E I_z \\over L^2} & 0 & 0 & 0 & {12 E I_z \\over L^3} & 0 & -{6 E I_z \\over L^2} & 0 \\\\
    0 & 0 & 0 & -{G J \\over L} & 0 & 0 & 0 & 0 & 0 & {G J \\over L} & 0 & 0 \\\\
    0 & 0 & {6 E I_z \\over L^2} & 0 & -{2 E I_z \\over L} & 0 & 0 & 0 & -{6 E I_z \\over L^2} & 0 & {4 E I_z \\over L} & 0 \\\\
    0 & {6 E I_y \\over L^2} & 0 & 0 & 0 & -{2 E I_y \\over L} & 0 & -{6 E I_y \\over L^2} & 0 & 0 & 0 & {4 E I_y \\over L} \\\\
    \\end{matrix} \\right]

Where :math:`A` is the area of the cross section, :math:`E` is the Young modulus
of the material, :math:`L` is the length of the element, :math:`I_y` and :math:`I_z`
are the moment of inertia of the cross section (as defined in :class:`soom.geometry.section`),
:math:`G` is the shear modulus, and :math:`J` is the polar moment of inertia.

Mass matrix
-----------

The mass matrix in local coordinates can be calculated using :meth:`Ebbeam.localm`.
The local stiffness matrix is given by the equation [Cook_et_al_2007]_ :

.. math::

    M = \\left[ \\begin{matrix}
    {\\gamma L \\over 3} & 0 & 0 & 0 & 0 & 0 & {\\gamma L \\over 6} & 0 & 0 & 0 & 0 & 0\\\\
    0 & {13 \\gamma L \\over 35} & 0 & 0 & 0 & {11 \\gamma L^2 \\over 210} & 0 & {9 \\gamma L \\over 70} & 0 & 0 & 0 & -{13 \\gamma L^2 \\over 420} \\\\
    0 & 0 & {13 \\gamma L \\over 35} & 0 & -{11 \\gamma L^2 \\over 210} & 0 & 0 & 0 & {9 \\gamma L \\over 70} & 0 & {13 \\gamma L^2 \\over 420} & 0 \\\\
    0 & 0 & 0 & {\\rho J L \\over 3} & 0 & 0 & 0 & 0 & 0 & {\\rho J L \\over 6} & 0 & 0 \\\\
    0 & 0 & -{11 \\gamma L^2 \\over 210} & 0 & {\\gamma L^3 \\over 105} & 0 & 0 & 0 & -{13 \\gamma L^2 \\over 420}  & 0 & -{\\gamma L^3 \\over 140} & 0 \\\\
    0 & {11 \\gamma L^2 \\over 210} & 0 & 0 & 0 & {\\gamma L^3 \\over 105} & 0 & {13 \\gamma L^2 \\over 420} & 0 & 0 & 0 & -{\\gamma L^3 \\over 140} \\\\
    {\\gamma L \\over 6} & 0 & 0 & 0 & 0 & 0 & {\\gamma L \\over 3} & 0 & 0 & 0 & 0 & 0 \\\\
    0 & {9 \\gamma L \\over 70} & 0 & 0 & 0 & {13 \\gamma L^2 \\over 420} & 0 & {13 \\gamma L \\over 35} & 0 & 0 & 0 & -{11 \\gamma L^2 \\over 210} \\\\
    0 & 0 & {9 \\gamma L \\over 70} & 0 & -{13 \\gamma L^2 \\over 420} & 0 & 0 & 0 & {13 \\gamma L \\over 35} & 0 & {11 \\gamma L^2 \\over 210} & 0 \\\\
    0 & 0 & 0 & {\\rho J L \\over 6} & 0 & 0 & 0 & 0 & 0 & {\\rho J L \\over 3} & 0 & 0 \\\\
    0 & 0 & {13 \\gamma L^2 \\over 420} & 0 & -{\\gamma L^3 \\over 140} & 0 & 0 & 0 & {11 \\gamma L^2 \\over 210} & 0 & {\\gamma L^3 \\over 105} & 0 \\\\
    0 & -{13 \\gamma L^2 \\over 420} & 0 & 0 & 0 & -{\\gamma L^3 \\over 140} & 0 & -{11 \\gamma L^2 \\over 210} & 0 & 0 & 0 & {\\gamma L^3 \\over 105} \\\\
    \\end{matrix} \\right]

Where :math:`\\gamma` is the mass density of the material, :math:`L` is the length
of the element, and :math:`\\rho = A \\gamma`, where :math:`A` is the cross sectional
area.

"""

import numpy as np

from .node import Node
from ..geometry.fline import Fline
from ..geometry.section import Section


class Ebbeam(Fline):
    """
    :class:`Ebbeam` is used to model beams in structures.

    Attributes:
        nodeList (list): List of nodes to build from.
        section (Section): Cross-section object.
        material (Isolinear): Material properties of the beam.
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 section,
                 material,
                 comment=""):
        """
        Constructor for Ebbeam objects.

        Args:
            nodeList (list): List of nodes to build from.
            section (Section): Cross-section object.
            material (Isolinear): Material properties of the beam.
            comment (str): Optional comments.
        """
        super(Ebbeam, self).__init__(nodeList=nodeList, comment=comment)
        self.section  = section
        self.material = material

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Ebbeam:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n------\n'
        out += str(self.section) + '\n------\n'
        out += str(self.material) + '\n------\n'
        out += "Ebbeam Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this Ebbeam instance as a string.

        Returns:
            str. The string representation of this Ebbeam instance.
        """
        return str(self)

    def localm(self):
        """
        Computes the local mass matrix of the beam.

        Returns:
            m (numpy.array): The local mass matrix.
        """
        # Material properties
        Rho = self.material.rho

        # Geometry properties
        Ar = self.section.area
        J = self.section.j

        # Lenght property
        L = self.length()

        # some additional terms
        gama = Rho*Ar
        I    = np.eye(12, 12)
        m    = np.zeros(shape=(12, 12))

        m[0, 0]   =     gama*L/3
        m[0, 6]   =     gama*L/6
        m[1, 1]   =  13*gama*L/35
        m[1, 5]   =  11*gama*L**2/210
        m[1, 7]   =   9*gama*L/70
        m[1, 11]  = -13*gama*L**2/420
        m[2, 2]   =  13*gama*L/35
        m[2, 4]   = -11*gama*L**2/210
        m[2, 8]   =   9*gama*L/70
        m[2, 10]  =  13*gama*L**2/420
        m[3, 3]   =    Rho*J*L/3
        m[3, 9]   =    Rho*J*L/6
        m[4, 4]   =     gama*L**3/105
        m[4, 8]   = -13*gama*L**2/420
        m[4, 10]  =    -gama*L**3/140
        m[5, 5]   =     gama*L**3/105
        m[5, 7]   =  13*gama*L**2/420
        m[5, 11]  =    -gama*L**3/140
        m[6, 6]   =     gama*L/3
        m[7, 7]   =  13*gama*L/35
        m[7, 11]  = -11*gama*L**2/210
        m[8, 8]   =  13*gama*L/35
        m[8, 10]  =  11*gama*L**2/210
        m[9, 9]   =    Rho*J*L/3
        m[10, 10] =     gama*L**3/105
        m[11, 11] =     gama*L**3/105
        m        = m+m.transpose()-np.multiply(I, m)
        return m

    def localk(self):
        """
        Computes the local stiffnes matrix of the beam.

        Returns:
            k (numpy.array): The local stiffness matrix.
        """
        # Material properties
        E = self.material.e
        G = self.material.g

        # Geometry properties
        Ar  = self.section.area
        i2  = self.section.iz
        i3  = self.section.iy
        i1  = self.section.j
 
        # Lenght property
        L = self.length()

        I = np.eye(12, 12)

        # c2  = 3*E*i2/(G*sa2*L**2);
        # c3  = 3*E*i3/(G*sa3*L**2);

        # Matrix elements
        A   = E*Ar/L
        T   = G*i1/L
        Q2  = 12*E*i3/L**3
        Q3  = 12*E*i2/L**3
        Q2m = 6*E*i3/L**2
        Q3m = 6*E*i2/L**2
        M2  = 4*E*i2/L
        M3  = 4*E*i3/L
        M2t = 2*E*i2/L
        M3t = 2*E*i3/L

        # k = np.matrix([[E*A/L, -E*A/L], [-E*A/L, E*A/L]])
        k = np.zeros(shape=(12, 12))

        k[0, 0]   = A
        k[0, 6]   = -A
        k[1, 1]   = Q2
        k[1, 5]   = -Q2m
        k[1, 7]   = -Q2
        k[1, 11]  = -Q2m
        k[2, 2]   = Q3
        k[2, 4]   = Q3m
        k[2, 8]   = -Q3
        k[2, 10]  = Q3m
        k[3, 3]   = T
        k[3, 9]   = -T
        k[4, 4]   = M2
        k[4, 8]   = -Q3m
        k[4, 10]  = M2t
        k[5, 5]   = M3
        k[5, 7]   = Q2m
        k[5, 11]  = M3t
        k[6, 6]   = A
        k[7, 7]   = Q2
        k[7, 11]  = Q2m
        k[8, 8]   = Q3
        k[8, 10]  = -Q3m
        k[9, 9]   = T
        k[10, 10]  = M2
        k[11, 11] = M3
        k         = k+k.transpose()-np.multiply(I, k)

        return k

    def globalm(self):
        """
        Calculates the global mass matrix

        Returns:
            M (numpy.array): Global mass matrix.
        """
        # Calculating transformation and local mass matrix
        T = self.tmatrix()
        m = self.localm()

        # Calculate global mass matrix
        M = np.dot(np.dot(T.T, m), T)

        return M

    def globalk(self):
        """
        Calculates the global stiffness matrix

        Returns:
            K (scipy.array): Global stiffness matrix
        """
        # Calculating transformation and local stiffness matrix
        T = self.tmatrix()
        k = self.localk()

        # Calculate global stiffness matrix
        K = np.dot(np.dot(T.T, k), T)

        return K

    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and stiffness

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*6, self.nodeList[0].number*6+6)
        dof += range(self.nodeList[1].number*6, self.nodeList[1].number*6+6)
        return dof

    def check(self):
        """
        Checks if the arguments of the ebeeam have been set correctly

        The arguments that are checked are:

        * nodeList is a list of 3 elements of type Node
        * section is of type section

        Returns:
            checkFlag (boolean): True if the check passes
        """
        checkFlag = True

        # Check that the nodeList is a list of 3 nodes
        if type(self.nodeList) is not list:
            print "The ebbeam.nodeList is expected to be a list (id %i)" % id(self)
            checkFlag = False
        elif len(self.nodeList) is not 3:
            print "The nodeList is expected to have 3 elements (id %i)" % id(self)
            checkFlag = False
        elif not all([isinstance(item, Node) for item in self.nodeList]):
            print "The elements of ebbeam.nodeList are expected to be Nodes (id %i)" % id(self)
            checkFlag = False

        # Checking that the section is type section
        if not isinstance(self.section, Section):
            print "The ebbeam.section is expected to be type section (id %i)" % id(self)
            checkFlag = False

        return checkFlag
