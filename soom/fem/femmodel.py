# -*- coding: utf-8 -*-
"""
FemModel
~~~~~~~~

:synopsis: Handles model structures.
"""

import scipy.sparse as sp
import numpy as np

class FemModel:
    """
    :class:`FemModel` is used to model structures.

    Attributes:
        elements (list): List of structural elements to build from.
        name (str): Name of this structure.
    """
    def __init__(self,
                 elementList,
                 name,
                 nodeList=[]):
        """
        Constructor for FemModel objects.

        Args:
            elementList (list): List of structural elements to build from.
            name (str): Name of this structure.
        """
        self.elementList = elementList
        self.name = name
        self.nodeList = nodeList

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = self.name + "\n"
        out += "Elements: " + str(len(self.elementList))
        return out

    def display(self):
        """
        Renders this FemModel instance as a string.

        Returns:
            str. The string representation of this FemModel instance.
        """
        return str(self)

    def plot(self,nodeNumber=False):
        """
        Plots this FemModel instance via Matplotlib.
        
        Args:
            nodeNumber (bool): True to plot the node numbers.  Default is False

        Returns:
            None.
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D # flake8: noqa

        # Set projection on current figure to 3D.
        fig = plt.gcf()
        fig.gca(projection='3d')

        # Plot element that has a plot method.
        for item in self.elementList:
            if "plot" in dir(item):
                item.plot()
                
        # Plot the nodes with their number
        if nodeNumber:
            for item in self.nodeList:
                item.plot(number=True)

    def numberNodes(self):
        """
        Extract all the nodes from the structural elements and renumber them

        Returns:
            None.
        """
        self.nodeList = []
        nodeNumber = 0

        # For each element in the list of elements
        for element in self.elementList:
            # Checking if the element has a property called "nodeList"
            if "nodeList" in dir(element):
                # For each of the nodes in the nodeList for this element
                for proposednode in element.nodeList:
                    # Check if the node is not already in the femmodel nodeList
                    if proposednode not in self.nodeList:
                        # Change the node number and add it to the femmodel.nodeList
                        proposednode.number = nodeNumber
                        self.nodeList.append(proposednode)
                        nodeNumber += 1

    def constMat(self):
        """
        Calculates the constrain matrix

        Returns:
            T (scipy.sparse.csc_matrix): Matrix that applies constrains
        """

        # List with all the bcs of nodes sorted
        ndof = len(self.nodeList) * 6
        bcs = []
        [bcs.extend(item.bc[:]) for item in sorted(self.nodeList, key=lambda node: node.number)]

        # Creating the transformation matrix
        rows = range(ndof)
        for j in range(ndof):
            if bcs[j]:
                rows.remove(j)

        cols = range(len(rows))
        vals = [1] * len(rows)

        T = sp.coo_matrix((vals, (rows, cols)),shape=(ndof, len(rows)))
        T = T.tocsc()

        return T

    def globalk(self, bc=True):
        """
        Calculates the stiffness matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            K (scipy.sparse.csc_matrix): Global stiffness matrix of the finite element model
        """


        # Initializing the stiffness matrix
        ndof = len(self.nodeList) * 6

        # scipy recommends building the matrix in lil or coo format and then
        # convert it to CSR or CSC for operations

        rows = []
        cols = []
        vals = []
        for element in self.elementList:
            # Check that the element an calculate a stiffness matrix
            if "globalk" in dir(element):
                # Get elemental matrix in global coordinates
                K_ele = element.globalk()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(K_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [K_ele[row, col] for row, col in zip(rowsele, colsele)]

        K = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        K = K.tocsc()

        # Applying boundary conditions
        if bc:
            T = self.constMat()
            K = np.dot(np.dot(T.transpose(),K),T)
            K = K.tocsc()

        # Applying Guyan reduction
        
        return K

    def check(self):
        """
        Check if the parameters of the FemModel were set correctly

        Returns:
            checkFlag (boolean): True if the point parameters are as expected

        This method calls the `check()` method in all the elements in
        elementList that have the method implemented.  This method also calls
        the `check()` method for all the nodes in nodeList if the list is
        not empty.
        """
        checkFlag = True

        # Check if the elementList is actually a list
        if type(self.elementList) is not list:
            print "The FemModel.elementList is not a list (id %i)" % id(self)
            checkFlag = False
        else:
            # Run a check in each element that has the check method implemented
            for element in self.elementList:
                if "check" in dir(element):
                    if not element.check():
                        checkFlag = False

        if len(self.nodeList) > 0:
            if not all([node.check() for node in self.nodeList]):
                checkFlag = False

        # self.elementList = elementList
        # self.nodeList = nodeList

        # Checking that the element
        return checkFlag

    def globalm(self, bc=True):
        """
        Calculates the mass matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            M (scipy.sparse.csc_matrix): Global mass matris of the finite element model
        """


        # Initializing the mass matrix
        ndof = len(self.nodeList) * 6

        rows = []
        cols = []
        vals = []

        for element in self.elementList:
            # Check that the element an calculate a stiffness matrix
            if "globalm" in dir(element):
                # Get elemental matrix in global coordinates
                M_ele = element.globalm()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(M_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [M_ele[row, col] for row, col in zip(rowsele, colsele)]

        M = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        M = M.tocsc()
        if bc:
            T = self.constMat()
            M = T.transpose() * M * T
            M = M.tocsc()

        return M

    def globalc(self, bc=True):
        """
        Calculates the damping matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            C (scipy.sparse.csc_matrix): Global damping matrix of the finite element model
        """
        

        # Initializing the stiffness matrix
        ndof = len(self.nodeList) * 6

        # scipy recommends building the matrix in lil or coo format and then
        # convert it to CSR or CSC for operations

        rows = []
        cols = []
        vals = []
        for element in self.elementList:
            # Check that the element an calculate a damping matrix
            if "globalc" in dir(element):
                # Get elemental matrix in global coordinates
                C_ele = element.globalc()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(C_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [C_ele[row, col] for row, col in zip(rowsele, colsele)]

        C = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        C = C.tocsc()

        # Applying boundary conditions
        if bc:
            T = self.constMat()
            C = T.transpose() * C * T
            C = C.tocsc()

        return C
        
    def ss(self, idof=0, odof=0, out='acc'):
        """
        Calculates the state space matrices of the finite element model

        Args:
            idof (int): input degree of freedom.
            odof (int): output degree of freedom.
            out (str): Output selection.
                          

        Returns:
            A, B, C, D (float64): State Space matrices
        """
        KK = self.globalk()
        CC = self.globalc()
        MM = self.globalm()
        
        #         
        C = CC.todense()        
        K = KK.todense()
        M = MM.todense()
                
        # Creating the complete state space matrices        
        
        Ass = np.zeros(shape=(len(M)*2,len(M)*2))
        Ass[0:len(M),len(M):len(M)*2] =  np.identity(len(M))
        Ass[len(M):len(M)*2,0:len(M)] = -np.dot(np.linalg.inv(M),K)
        Ass[len(M):len(M)*2,len(M):len(M)*2] = -np.dot(np.linalg.inv(M),C)
        
        Bss = np.zeros(shape=(len(M)*2,len(M)))
        Bss[len(M):len(M)*2,0:len(M)] = np.linalg.inv(M)
        
        Css = np.zeros(shape=(len(M),len(M)*2))
        
        for x in out:
            if x in 'dis':
                Css[0:len(M),0:len(M)] =  np.identity(len(M))
                Css[0:len(M),len(M):len(M)*2] = np.zeros(shape=(len(M),len(M)))
                
                Dss = np.zeros(shape=(len(M),len(M)))
        
            if x in 'vel':
                Css[0:len(M),0:len(M)] =  np.zeros(shape=(len(M),len(M)))
                Css[0:len(M),len(M):len(M)*2] = np.identity(len(M))
                
                Dss = np.zeros(shape=(len(M),len(M)))

            if x in 'acc':
                Css[0:len(M),0:len(M)] = -np.dot(np.linalg.inv(M),K)
                Css[0:len(M),len(M):len(M)*2] = -np.dot(np.linalg.inv(M),C)
                
                Dss = np.linalg.inv(M)

        # getting the dof selected for input and output

        if idof == 0 and odof == 0:
            As = Ass
            Bs = Bss
            Cs = Css
            Ds = Dss
        else:
            Idof = idof-1
            Odof = odof-1
            
            As = Ass
            Bs = np.array([Bss[:,Idof]]).T
            Cs = Css[Odof][:]
            Ds = Dss[Odof,Idof]

        return As, Bs, Cs, Ds

    def modal(self, n=0):
        """
        Calculates the natural frequencies freq (Hz) and mode shape associate to each frequency of the model.

        Args:
            n (int): number of eigenvalues and eigenvectors to be returned.
                          
        Returns:
            w : frequencies (rad/s)
            v    : mode shape vectors
        """
        
        from numpy import linalg as LA
        import numpy as np
        

        K = self.globalk()
        M = self.globalm()
        
        w, v = LA.eig(np.dot(np.linalg.inv(M.todense()),K.todense()))
        
        # Sorting
        indx = np.argsort(w)
        w = w[indx]
        v = v[:,indx]

        # Taking the square root of omegas to return frequencies in rad/s
        w = np.sqrt(w)

        # Returning only the modes requested        
        if n != 0:
            w = w[0:n]
            v = v[:,0:n]
            
        return w, v