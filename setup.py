try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name='soom',
    version='0.0.1-alpha',
    url='https://bitbucket.org/sdii/soompy',
    license='Apache License, version 2',
    author='Structural Dynamics and Intelligent Infrastructure Lab',
    maintainer='Juan M. Caicedo',
    maintainer_email='caicedo@cec.sc.edu',
    description='Structural Object-Oriented Modeling in Python',
    # long_description=__doc__,
    packages=[
        'soom',
        'soom.geometry',
        'soom.fem',
        'soom.materials'
    ],
    include_package_data=True,
    zip_safe=True,
    platforms='any',
    install_requires=[
        'numpy',
        'scipy',
        'nose',
    ],
    extras_require={
        'doc': ["Sphinx>=1.2.2", "sphinxcontrib-napoleon>=0.2.8"],
        'plot': ["Matplotlib>=1.3.1"]
    }
)
