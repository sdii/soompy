# -*- coding: utf-8 -*-
"""
To run this test use py.test
"""

from soom.fem.ebbeam import Ebbeam
from soom.materials.isolinear import Isolinear 
from soom.geometry.section import Section
from soom.fem.node import Node
import numpy as np
import soom as sp


def test_globalk():

    #%% Constants
    E = 29e6
    G = (29e6)/(2.6)
    Iy = 10.6
    Iz = 7.48 
    J= 14 
    L = 17
    A = 2.93
    #I    = np.eye(12, 12) 
    #%%Manual calculations
    k_test = np.zeros((12,12))
    k_test[0,0] = A*E/L
    k_test[0,6] = -A*E/L
    k_test[1,1] = 12*E*Iy/L**3
    k_test[1,5] = -6*E*Iy/L**2
    k_test[1,7] = -12*E*Iy/L**3
    k_test[1,11] = -6*E*Iy/L**2
    k_test[2,2] = 12*E*Iz/L**3
    k_test[2,4] = 6*E*Iz/L**2
    k_test[2,8] = -12*E*Iz/L**3
    k_test[2,10] = 6*E*Iz/L**2
    k_test[3,3] = G*J/L
    k_test[3,9] = -G*J/L
    k_test[4,2] = 6*E*Iz/L**2
    k_test[4,4] = 4*E*Iz/L
    k_test[4,8] = -6*E*Iz/L**2
    k_test[4,10] = 2*E*Iz/L
    k_test[5,1] = -6*E*Iy/L**2
    k_test[5,5] = 4*E*Iy/L
    k_test[5,7] = 6*E*Iy/L**2
    k_test[5,11] = 2*E*Iy/L
    k_test[6,0] =-A*E/L
    k_test[6,6] = A*E/L
    k_test[7,1] = -12*E*Iy/L**3
    k_test[7,5] = 6*E*Iy/L**2
    k_test[7,7] = 12*E*Iy/L**3
    k_test[7,11] = 6*E*Iy/L**2
    k_test[8,2] = -12*E*Iz/L**3
    k_test[8,4] = -6*E*Iz/L**2
    k_test[8,8] = 12*E*Iz/L**3
    k_test[8,10] = -6*E*Iz/L**2
    k_test[9,3] = -G*J/L
    k_test[9,9] = G*J/L
    k_test[10,2] = 6*E*Iz/L**2
    k_test[10,4] = 2*E*Iz/L
    k_test[10,8] = -6*E*Iz/L**2
    k_test[10,10] = 4*E*Iz/L
    k_test[11,1] = -6*E*Iy/L**2
    k_test[11,5] = 2*E*Iy/L
    k_test[11,7] = 6*E*Iy/L**2
    k_test[11,11] = 4*E*Iy/L
   
    
    
    #%% Building the structural system
    nlist = [ Node([0,0,0],number=0), Node([17,0,0],number=1), Node([17,1,0],number=2)] 
    s1 = Section(area=2.93, iy=10.6, iz=7.48, j=14)
    mat = Isolinear(name = "Steel",e = 29e6, rho = 0.284, nu=0.3)
    beam = Ebbeam(nlist, s1, mat)
    k = beam.localk()
  
    #beam.plot()
    #print (k)
    
    #%% Compare
    k_comp = k - k_test
    
    if not k_comp.any():  # If all values are zero
        assert True
    else:
        indx = k_comp.nonzero()

        for row,col in zip(indx[0],indx[1]):
            print "The element %i, %i of the local stiffness matrix is not correct"%(row,col)
        assert False
        