@echo off

cd ..
rmdir /S /Q build
rmdir /S /Q dist
rmdir /S /Q soom.egg-info
cd scripts