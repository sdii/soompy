.. automodule:: soom.fem.lspring

    Class Description
    -----------------
    .. autoclass:: Lspring
       :members:
       :private-members:
       :special-members:
