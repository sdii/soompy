.. automodule:: soom.fem.femmodel

    Class Description
    -----------------
    .. autoclass:: FemModel
       :members:
       :private-members:
       :special-members: