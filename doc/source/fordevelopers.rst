Information for Developers
~~~~~~~~~~~~~~~~~~~~~~~~~~

This goal of this document is to provide an overview for those users that would like to create their own code that will enhance the capabilities of SOOMpy.  For example, if you would like to add a new finite element.  This document assumes that you have some experience with python and gives information about how to develop your classes to they will inter-operate with other SOOMpy classes.

Adding new FEM elements
=======================

New finite elements can be easily coded into SOOMpy.  Depending on the type of element different methods should be implemented.  For example, any element that contributes to the model stiffness should have a :meth:`globalk` method.  Any element that adds to the mass of the system should have a :meth:`globalm` method.  Finally any element that contributes to the damping matrix, such as viscous dampers should have a :meth:`globalc` method.

The FemModel performs the matrix assembly from all elements.  However, the element should aid the FemModel class perform the assembly.  This is performed by indicating the DOF in which the stiffness matrix should be assembled via the :meth:`dofList` method.

For example, consider the :class:`soom.fem.ebbeam` class.  The element uses 3 nodes.  The first two are used to indicate the ends of the beam and the third node to determine the orientation of the cross section.  The stiffness matrix returned from :meth:`soom.fem.ebbeam.globalk` is a 6x6 matrix.  The list returned from :meth:`soom.fem.ebbeam.dofList` will be of length 6.  These DOF will coincide with the global DOF in which the stiffness matrix should be assembled.

The truss element
-----------------

Consider, for example, that a truss element will be developed.  For simplicity let's consider that this element will use :class:`soom.materials.isolinear` material and will have a constant cross section defined by :class:`soom.geometry.section`.  The python file defining this class will be located in soom/fem/.  This element can inherent some properties of the :class:`soom.geometry.fline` that enables plotting and other common tasks for "linear" elements.  To inherent the properties and methods of :class:`soom.geometry.fline` use the following syntax when defining your class::

    import numpy as np
    from ..geometry.fline import Fline
    
        class Truss(Fline)

Other elements like :class:`soom.fem.ebbeam` have a :meth:`localk` method to break the formulation of the stiffness matrix in two steps.  The :meth:`localk` method builds the matrix in local coordinates and the :meth:`globalk` method transfors it to global coordinates.  The :meth:`localk` method for the trus class can be formulated as follow::

    def localk(self):
        """
	Computes the local stiffnes matrix of the truss.

	Returns:
	    k (numpy.array): The local stiffness matrix.
        """
	# Young modulus
	E = self.material.e

	# Area of the cross sectional section
	A  = self.section.area

	# Lenght
	L = self.length()

	alpha = A*E/L
        
	k = np.zeros(shape=(6, 6))
	k[0,0] = alpha
	k[0,3] = -alpha
	k[3,0] = -alpha
	k[3,3] = alpha
        
	return k


Here it is assummed that the property :attr:`material` is an instance of :class:`soom.materials.isolinear`.  Similarly, the :attr:`section` property is an instance of :class:`soom.geometry.section`.  The :meth:`length` method is inherited from the :class:`soom.geometry.fline` class.  The matrix returned is a 3x3 :class:`numpy.array`.

The next step in the implementation of this class is to write the :meth:`globalk` method.  This can be done using the following code::

    def globalk(self):
        """
        Calculates the global stiffness matrix

        Returns:
            K (scipy.array): Global stiffness matrix
        """
        # Calculating transformation and local stiffness matrix
        T = self.tmatrix()
        
        # Select the appropriate parts of the transformation matrix
        T = np.delete(T,[3, 4, 5, 9, 10, 11],0)
        T = np.delete(T,[3, 4, 5, 9, 10, 11],1)
        
        # Calculate the stiffness matrix in local coordinates
        k = self.localk()

        # Calculate global stiffness matrix
        K = np.dot(np.dot(T.T, k), T)

        return K

Notice that the resulting stiffness matrix is 6x6.  This could be of different sizes depending on the element.  For example, the :class:`soom.fem.ebbeam` returns a 12x12 matrix.  Another method is needed to help the :class:`soom.fem.femmodel` class know where to assemble the corresponding matrix.  This is done with the :meth:`dofList` method::

    def dofList(self):
    """
    Determines the DOF that contribute to the element's mass and stiffness
    
    Returns:
        dof (list): List of DOF
    """

    dof = range(self.nodeList[0].number*3,self.nodeList[0].number*3+3)
    dof += range(self.nodeList[1].number*3, self.nodeList[1].number*3+3)
    return dof
    
The method above will return a list corresponding to the DOF in which the matrix should be assambled.
