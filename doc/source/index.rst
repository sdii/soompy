.. SOOMpy documentation master file, created by
   sphinx-quickstart on Tue Jun 03 13:31:48 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


SOOMpy Documentation
====================

Introduction
------------
Structural Object Oriented Programming (SOOM) is a programming platform designed to allow researchers and engineers to test algorithms for simulation, modeling, and model updating, among other uses.
SOOM is different from other programs such as OpenSees because it does not seek to provide a whole computational simulation framework.
Rather, it looks to provide a modeling environment to allow researchers and engineers to build numerical modeling tools.

Though SOOM was originally built for FEM (Finite Element Modeling) work, it is not strictly constrained to that domain.

SOOM was originally written in Matlab--SOOMpy is the continuation of that project in Python.

Topics
------

.. toctree::
   :maxdepth: 2

   quickstart
   fordevelopers
   nomenclature


Current Modules
---------------

.. toctree::
   :maxdepth: 2

   materials
   geometry
   fem


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

