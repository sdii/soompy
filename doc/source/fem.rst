FEM
===
The :mod:`soom.fem` module allows one to work with FEM (Finite Element Modelling) objects.

.. toctree::
   :maxdepth: 2

   modules/soom.fem.node
   modules/soom.fem.truss
   modules/soom.fem.ebbeam
   modules/soom.fem.lmass
   modules/soom.fem.vdamper
   modules/soom.fem.lspring
   modules/soom.fem.femmodel
